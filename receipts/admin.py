from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)


# @admin.register(ExpenseCategory)
# class ExpenseCategoryAdmin(admin.ModelAdmin):
#     list_display = (
#         "name",)


# @admin.register(Account)
# class Account(admin.ModelAdmin):
#     list_display = (
#         "name",
#         "number",
#     )


# @admin.register(Receipt)
# class Receipt(admin.ModelAdmin):
#     list_display = (
#         "vendor",
#         "total",
#         "tax",
#         "date",
#         "purchaser",
#         "category",
#         "account",
#     )

from django.urls import path
from .views import login
from .views import user_logout, signup

urlpatterns = [
    path("login/", login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
